variable "minimum_password_length" {
  description = "Minimum password length of User"
  type        = number
  default     = 6
}

variable "require_lowercase_characters" {
  description = "Whether lowercase chars are required"
  type        = bool
  default     = true
}

variable "require_numbers" {
  description = "Whether numbers are required"
  type        = bool
  default     = true
}

variable "require_uppercase_characters" {
  description = "Whether uppercase chars are required"
  type        = bool
  default     = true
}

variable "require_symbols" {
  description = "Whether symbos are required"
  type        = bool
  default     = true
}

variable "allow_users_to_change_password" {
  description = "Allow users to change their passwords when user loged in first time"
  type        = bool
  default     = true
}

variable "password_reuse_prevention" {
  description = "The number of previous passwords that users are prevented from reusing"
  type        = number
  default     = 2
}

variable "max_password_age" {
  description = "The number of days that an user password is valid"
  type        = number
  default     = 1
}