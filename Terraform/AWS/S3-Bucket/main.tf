resource "aws_s3_bucket" "bucket_indore" {
  bucket = "loves-terraform"
  acl           = "private"

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "block_bucket" {
  bucket = aws_s3_bucket.bucket_indore.id

  block_public_acls         = true
  block_public_policy       = true
#   ignore_public_acls        = true
  restrict_public_buckets   = true
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.bucket_indore.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "MYBUCKETPOLICY",
  "Statement": [
    {
      "Sid": "",
      "Action": [
        "s3:List*",
        "s3:Put*"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::loves-terraform/*",
      "Principal": "*"
    }
  ]
}
POLICY
}