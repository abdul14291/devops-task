# Note:- Hear we will going to create two public ip for our VMs
resource "azurerm_public_ip" "IndoreIP" {
    count               = var.resource_count
    name                = "${var.resource_name}-${format("%02d", count.index)}IndoreIP"
    location            = azurerm_resource_group.IndoreMP.location
    resource_group_name = azurerm_resource_group.IndoreMP.name
    allocation_method   = var.Environment == "Production" ? "Static":"Dynamic"  
}

# Hear we will going to create one public ip for our Load Balancer
resource "azurerm_public_ip" "IPforLB" {
    name                = "IPforLoadBalancer"
    location            = azurerm_resource_group.IndoreMP.location
    resource_group_name = azurerm_resource_group.IndoreMP.name
    allocation_method   = "Static"
}