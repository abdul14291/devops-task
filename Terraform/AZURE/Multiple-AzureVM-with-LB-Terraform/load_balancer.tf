resource "azurerm_lb" "IndoreMPLB" {
 name                = var.load_balancer
 location            = azurerm_resource_group.IndoreMP.location
 resource_group_name = azurerm_resource_group.IndoreMP.name

 frontend_ip_configuration {
   name                 = "PublicIPAddress"
   public_ip_address_id = azurerm_public_ip.IPforLB.id
 }

 tags = {
        environment = "Terraform Demo"
    }
}
