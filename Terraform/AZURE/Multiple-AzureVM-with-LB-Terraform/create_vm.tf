resource "azurerm_virtual_machine" "IndoreVM" {
    count                   = var.resource_count
    name                    = "${var.resource_name}-${format("%02d", count.index)}"
    location                = azurerm_resource_group.IndoreMP.location
    availability_set_id     = azurerm_availability_set.IndoreMPAVSet.id
    resource_group_name     = azurerm_resource_group.IndoreMP.name
    network_interface_ids   = [element(azurerm_network_interface.IndoreNIC.*.id, count.index)]
    vm_size                 = "Standard_B1s"
    delete_os_disk_on_termination = true

# Note:- Hear we will find image from this Azure website:- https://docs.microsoft.com/en-us/azure/virtual-machines/linux/cli-ps-findimage
    storage_image_reference {
        publisher   = "Canonical"
        offer       = "UbuntuServer"
        sku         = "18.04-LTS"
        version     = "latest"
    }

# Note:- This is Extra Disk will attach with our VM.
    storage_os_disk {
        name = "MyOsDisk-${count.index}"
        caching = "ReadWrite"
        create_option = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    os_profile {
        computer_name = "MyIndoreVM"
        admin_username = var.admin_user
        admin_password  = var.admin_password
    }

    os_profile_linux_config {
        disable_password_authentication = false
    }

     tags = {
        environment = "Terraform Demo"
    }
}