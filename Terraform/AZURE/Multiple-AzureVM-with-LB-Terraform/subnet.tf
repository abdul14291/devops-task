resource "azurerm_subnet" "IndoreSubnet" {
    name                    = "${var.resource_name}-IndoreSubnet"
    resource_group_name     = azurerm_resource_group.IndoreMP.name
    virtual_network_name    = azurerm_virtual_network.IndoreNetwork.name
    address_prefixes        = var.subnet_prefixes  
}