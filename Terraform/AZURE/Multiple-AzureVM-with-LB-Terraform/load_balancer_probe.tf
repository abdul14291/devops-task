resource "azurerm_lb_probe" "IndoreMPLBProbe" {
 resource_group_name = azurerm_resource_group.IndoreMP.name
 loadbalancer_id     = azurerm_lb.IndoreMPLB.id
 name                = "ssh-running-probe"
 port                = var.application_port
}