resource "azurerm_lb_rule" "IndoreLBRule" {
   resource_group_name            = azurerm_resource_group.IndoreMP.name
   loadbalancer_id                = azurerm_lb.IndoreMPLB.id
   name                           = "http"
   protocol                       = "Tcp"
   frontend_port                  = var.application_port
   backend_port                   = var.application_port
   backend_address_pool_id        = azurerm_lb_backend_address_pool.IndoreMPLBPool.id
   # Note:- Hear we will type "frontend_ip_configuration_name" which we wrote in load_balancer.tf file
   frontend_ip_configuration_name = "PublicIPAddress"
   probe_id                       = azurerm_lb_probe.IndoreMPLBProbe.id
}