variable "location" {
    type = string
}

variable "resource_name" {
    type = string
}

variable "virtual_network" {
    default = ["10.0.0.0/16"]  
}

#variable for network range

variable "subnet_prefixes" {
    default = ["10.0.1.0/24"]
}

#v ariable for Environment

variable "Environment" {
    type = string
}

variable "resource_count" {
    type = number
}

# variable for LoadBalancer

variable "load_balancer" {
    type = string
}

variable "application_port" {
   type = string
}

# Set Admin User Name or Password

variable "admin_user" {
   type = string
}

variable "admin_password" {
   type  =  string
}

# variable for Avalibility Set

variable "availibility_set" {
    type = string
}