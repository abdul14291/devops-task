resource "azurerm_network_interface" "NICforPublicVM" {
 name                = "NetworkInterface-for-PublicVM"
 location            = var.location
 resource_group_name = azurerm_resource_group.Indore.name

 ip_configuration {
   name                          = "MyIPConfiguration"
   subnet_id                     = azurerm_subnet.IndoreSubnet.id
   private_ip_address_allocation = "dynamic"
   public_ip_address_id          = azurerm_public_ip.IPforPublicVM.id
 }

 tags = var.tags
}