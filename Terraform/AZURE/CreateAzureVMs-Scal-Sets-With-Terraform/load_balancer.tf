resource "azurerm_lb" "IndoreLB" {
 name                = "IndoreMPLB"
 location            = var.location
 resource_group_name = azurerm_resource_group.Indore.name

 frontend_ip_configuration {
   name                 = "PublicIPAddress"
   public_ip_address_id = azurerm_public_ip.IPforLBPrivateVM.id
 }

 tags = var.tags
}
