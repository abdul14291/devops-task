resource "azurerm_virtual_machine_scale_set" "PrivateVM" {
 name                = "Create-Private-VM"
 location            = var.location
 resource_group_name = azurerm_resource_group.Indore.name
 upgrade_policy_mode = "Manual"

 sku {
   name     = "Standard_B1s"
   tier     = "Standard"
   capacity = 2
 }

 storage_profile_image_reference {
   publisher = "Canonical"
   offer     = "UbuntuServer"
   sku       = "18.04-LTS"
   version   = "latest"
 }

# Note:- This is disk inside this disk our OS will be install
 storage_profile_os_disk {
# Note:-  We have to set name = "" as BLANCK this is standard, do not type
   name              = ""
   caching           = "ReadWrite"
   create_option     = "FromImage"
   managed_disk_type = "Standard_LRS"
 }

 os_profile {
   computer_name_prefix = "PrivateVM"
   admin_username       = var.admin_user
   admin_password       = var.admin_password
   custom_data          = file("web.conf")
 }

 os_profile_linux_config {
   disable_password_authentication = false
 }

 network_profile {
   name    = "MyNetworkProfile"
   primary = true

   ip_configuration {
     name                                   = "MyIPConfiguration"
     subnet_id                              = azurerm_subnet.IndoreSubnet.id
     load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.IndoreLBPool.id]
     primary = true
   }
 }

 tags = var.tags
}

# With help of this Public VM we will access our PrivateVM with SSH
# This VM we will use AS JumpServer
resource "azurerm_virtual_machine" "PublicVM" {
 name                  = "Create-PublicVM"
 location              = var.location
 availability_set_id   = azurerm_availability_set.IndoreAVSet.id
 resource_group_name   = azurerm_resource_group.Indore.name
 network_interface_ids = [azurerm_network_interface.NICforPublicVM.id]
 vm_size               = "Standard_B1s"

 storage_image_reference {
   publisher = "Canonical"
   offer     = "UbuntuServer"
   sku       = "18.04-LTS"
   version   = "latest"
 }

 storage_os_disk {
   name              = "MyOSDisk"
   caching           = "ReadWrite"
   create_option     = "FromImage"
   managed_disk_type = "Standard_LRS"
 }

 os_profile {
   computer_name  = "PublicVM"
   admin_username = var.admin_user
   admin_password = var.admin_password
 }

 os_profile_linux_config {
   disable_password_authentication = false
 }

 tags = var.tags
}