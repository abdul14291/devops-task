resource "azurerm_lb_probe" "IndoreLBProbe" {
 resource_group_name = azurerm_resource_group.Indore.name
 loadbalancer_id     = azurerm_lb.IndoreLB.id
 name                = "ssh-running-probe"
 port                = var.application_port
}