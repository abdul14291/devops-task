# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "IndoreSGAssociate" {
    network_interface_id      = azurerm_network_interface.NICforPublicVM.id
    network_security_group_id = azurerm_network_security_group.IndoreSG.id
}