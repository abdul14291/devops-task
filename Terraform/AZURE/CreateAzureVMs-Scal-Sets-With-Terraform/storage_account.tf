# Create storage account for boot diagnostics
resource "azurerm_storage_account" "INDSACC" {
    # name                        = "ind${random_id.IndoreID.hex}"
    # Note:- StorageAccount Name should be unic and in small latter
    name                        = "indoresacc78468"
    resource_group_name         = azurerm_resource_group.Indore.name
    location                    = "eastus"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        environment = "Terraform Demo"
    }
}