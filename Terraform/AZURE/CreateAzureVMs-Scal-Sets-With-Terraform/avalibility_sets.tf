# Note:- AvalibilitySet Will be Attached only PublicVm
resource "azurerm_availability_set" "IndoreAVSet" {
 name                         = "IndoreMPAVSet"
 location                     = azurerm_resource_group.Indore.location
 resource_group_name          = azurerm_resource_group.Indore.name
 platform_fault_domain_count  = 2
 platform_update_domain_count = 2
 managed                      = true
}