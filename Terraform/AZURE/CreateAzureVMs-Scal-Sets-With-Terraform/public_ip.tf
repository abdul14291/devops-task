resource "azurerm_public_ip" "IPforLBPrivateVM" {
 name                         = "IP-For-Load-Balancer-PrivateVM"
 location                     = var.location
 resource_group_name          = azurerm_resource_group.Indore.name
 allocation_method            = "Static"
 domain_name_label            = random_string.fqdn.result
 tags                         = var.tags
}

resource "azurerm_public_ip" "IPforPublicVM" {
 name                         = "IP-For-PublicVM"
 location                     = var.location
 resource_group_name          = azurerm_resource_group.Indore.name
 allocation_method            = "Static"
 domain_name_label            = "${random_string.fqdn.result}-ssh"
 tags                         = var.tags
}