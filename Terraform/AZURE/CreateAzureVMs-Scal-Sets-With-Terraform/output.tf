output "PrivateVM_public_ip" {
     value = azurerm_public_ip.IPforLBPrivateVM.fqdn
 }

 output "PublicVM_public_ip" {
   value = azurerm_public_ip.IPforPublicVM.fqdn
}