resource "azurerm_virtual_network" "IndoreNetwork" {
    name                = "${var.resource_name}-IndoreNetwork"
    resource_group_name = azurerm_resource_group.IndoreMP.name
    location            = var.location
    address_space       = var.subnet_prefixes  
}