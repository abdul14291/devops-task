resource "azurerm_network_interface" "IndoreNIC" {
    count               = var.resource_count
    name                = "${var.resource_name}-${format("%02d", count.index)}-IndoreNIC"
    location            = azurerm_resource_group.IndoreMP.location
    resource_group_name = azurerm_resource_group.IndoreMP.name  

    ip_configuration {
        name            = "myinodreip"
        subnet_id       = azurerm_subnet.IndoreSubnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id = element(azurerm_public_ip.IndoreIP.*.id, count.index)
    }
}