# Create a resource group
resource "azurerm_resource_group" "IndoreMP" {
    name        = "${var.resource_name}-IndoreMP"
    location    = var.location
}