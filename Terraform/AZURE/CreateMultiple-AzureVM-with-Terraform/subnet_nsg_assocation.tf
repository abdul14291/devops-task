# Subnet and NSG association 

resource "azurerm_subnet_network_security_group_association" "IndoreSB_NSG_Association" {
    # network_interface_id      = azurerm_network_interface.IndoreNIC.id
    subnet_id                   = azurerm_subnet.IndoreSubnet.id
    network_security_group_id   = azurerm_network_security_group.IndoreNSG.id
}