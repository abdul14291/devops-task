resource "azurerm_managed_disk" "IndoreDisk" {
 name                 = var.manage_disk
 location             = "eastus"
 resource_group_name  = azurerm_resource_group.Indore.name
 storage_account_type = "Standard_LRS"
 create_option        = "Empty"
 disk_size_gb         = "50"
}