# Create public IPs
resource "azurerm_public_ip" "IndoreIP" {
    name                         = var.public_ip
    location                     = "eastus"
    resource_group_name          = azurerm_resource_group.Indore.name
    allocation_method            = "Dynamic"

    tags = {
        environment = "Terraform Demo"
    }
}