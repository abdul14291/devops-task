resource "azurerm_availability_set" "IndoreAVSet" {
 name                         = var.avset
 location                     = "eastus"
 resource_group_name          = azurerm_resource_group.Indore.name
 platform_fault_domain_count  = 2
 platform_update_domain_count = 2
 managed                      = true
}