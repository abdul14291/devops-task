# Create a resource group if it doesn't exist
resource "azurerm_resource_group" "Indore" {
    name     = var.rg_name
    location = "eastus"

    tags = {
        environment = "Terraform Demo"
    }
}