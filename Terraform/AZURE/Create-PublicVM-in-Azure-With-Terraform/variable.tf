# variable "location" {
#     type = string
# }

variable "rg_name" {
    type = string
}

variable "subnet" {
    type = string
}

variable "public_ip" {
    type = string
}

variable "vnet" {
    type = string
}

variable "manage_disk" {
    type = string
}

variable "avset" {
    type = string
}

variable "nic" {
    type = string
}

variable "sg" {
    type = string
}

# variable "subnet_rang" {
#     type = string
# }

# variable "vnet_rang" {
#     type = string
# }

variable "user_admin" {
    type = string
}

variable "user_pass" {
    type = string
}
