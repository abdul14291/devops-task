# Please note that the assignment takes around 30 minutes to take effect.
resource "azurerm_policy_definition" "policy" {
  name         = "AddReplaceTag"
  policy_type  = "Custom"
  mode         = "Indexed"
  display_name = "Add or Replace Tag from Resource"

metadata = <<METADATA
  {
  "category": "General"
  }

METADATA

   policy_rule = <<POLICY_RULE
    {
    "if": {
      "field": "[concat('tags[', parameters('tagName'), ']')]",
      "notEquals": "[parameters('tagValue')]"
    },
    "then": {
      "effect": "modify",
      "details": {
        "roleDefinitionIds": [
          "/providers/microsoft.authorization/roleDefinitions/b24988ac-6180-42a0-ab88-20f7382dd24c"
        ],
        "operations": [
          {
            "operation": "addOrReplace",
            "field": "[concat('tags[', parameters('tagName'), ']')]",
            "value": "[parameters('tagValue')]"
          }
        ]
      }
    }
  }
  POLICY_RULE

  parameters = <<PARAMETERS
  {
    "tagName": {
      "type": "String",
      "metadata": {
        "displayName": "Tag Name",
        "description": "Name of the tag, such as 'environment'"
      }
    },
    "tagValue": {
      "type": "String",
      "metadata": {
        "displayName": "Tag Value",
        "description": "Value of the tag, such as 'production'"
      }
    }
  }
PARAMETERS
}