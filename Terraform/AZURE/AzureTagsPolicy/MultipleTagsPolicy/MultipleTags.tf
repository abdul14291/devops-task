resource "azurerm_policy_definition" "policy" {
  name         = "MyCustomPolicy"
  policy_type  = "Custom"
  mode         = "Indexed"
  display_name = "MyCustomPolicyForMultipleTag"

metadata = <<METADATA
  {
  "category": "General"
  }

METADATA

    policy_rule = <<POLICY_RULE
    {
    "if": {
      "anyOf": [
        {
          "field": "[concat('tags[', parameters('orderby'), ']')]",
          "exists": "false"
        },
        {
          "field": "[concat('tags[', parameters('assignedby'), ']')]",
          "exists": "false"
        },
        {
          "field": "[concat('tags[', parameters('createby'), ']')]",
          "exists": "false"
        }
      ]
    },
    "then": {
      "effect": "deny"
    }
  }
  POLICY_RULE
  parameters = <<PARAMETERS
  {
    "orderby": {
      "type": "String",
      "metadata": {
        "displayName": "TagName1",
        "description": "who will give the order to create this resourece 'orderby'"
      }
    },
    "assignedby": {
      "type": "String",
      "metadata": {
        "displayName": "TagName2",
        "description": "who is assign this task to you 'teamlead'"
      }
    },
    "createby": {
      "type": "String",
      "metadata": {
        "displayName": "TagName3",
        "description": "who will creat this resource 'createby'"
      }
    }
  }
PARAMETERS
}