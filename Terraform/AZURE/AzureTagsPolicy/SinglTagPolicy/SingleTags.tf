resource "azurerm_policy_definition" "policy" {
  name         = "MyCustomPolicy"
  policy_type  = "Custom"
  mode         = "Indexed"
  display_name = "MyCustomPolicyForSinglTag"

metadata = <<METADATA
  {
  "category": "General"
  }

METADATA

  policy_rule = <<POLICY_RULE
  {
    "if": {
      "not": {
        "field": "[concat('tags[', parameters('tagName'), ']')]",
        "equals": "[parameters('tagValue')]"
      }
    },
    "then": {
      "effect": "deny"
    }
  }
  POLICY_RULE

  parameters = <<PARAMETERS
  {
    "tagName": {
      "type": "String",
      "metadata": {
        "displayName": "Tag Name",
        "description": "Name of the tag, such as 'environment'"
      }
    },
    "tagValue": {
      "type": "String",
      "metadata": {
        "displayName": "Tag Value",
        "description": "Value of the tag, such as 'production'"
      }
    }
  }
PARAMETERS
}
