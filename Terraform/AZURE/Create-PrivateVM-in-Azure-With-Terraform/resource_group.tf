# Create a resource group if it doesn't exist
resource "azurerm_resource_group" "Indore" {
    name     = "IndoreMP"
    location = "eastus"

    tags = {
        environment = "Terraform Demo"
    }
}