resource "azurerm_managed_disk" "IndoreDisk" {
 name                 = "IndoreMPDisk"
 location             = azurerm_resource_group.Indore.location
 resource_group_name  = azurerm_resource_group.Indore.name
 storage_account_type = "Standard_LRS"
 create_option        = "Empty"
 disk_size_gb         = "50"
}