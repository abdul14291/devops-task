# Create virtual network
resource "azurerm_virtual_network" "IndoreNetwork" {
    name                = "IndoreMPNetwork"
    address_space       = ["10.0.0.0/16"]
    location            = "eastus"
    resource_group_name = azurerm_resource_group.Indore.name

    tags = {
        environment = "Terraform Demo"
    }
}