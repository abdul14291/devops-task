# Create network interface
resource "azurerm_network_interface" "IndoreNIC" {
    name                      = "IndoreMPNIC"
    location                  = "eastus"
    resource_group_name       = azurerm_resource_group.Indore.name

    ip_configuration {
        name                          = "MyIP"
        subnet_id                     = azurerm_subnet.IndoreSubnet.id
        private_ip_address_allocation = "Dynamic"
        # Note:- If want to only private IP note Public then Comment this Public_ip_address_id line.
        # public_ip_address_id          = azurerm_public_ip.IndoreIP.id
    }

    tags = {
        environment = "Terraform Demo"
    }
}