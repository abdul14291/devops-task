# Generate random text for a unique storage account name
resource "random_id" "IndoreID" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = azurerm_resource_group.Indore.name
    }

    byte_length = 4
}