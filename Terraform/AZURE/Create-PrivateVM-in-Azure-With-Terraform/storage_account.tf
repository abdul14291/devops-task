# Create storage account for boot diagnostics
resource "azurerm_storage_account" "INDSACC" {
    name                        = "ind${random_id.IndoreID.hex}"
    resource_group_name         = azurerm_resource_group.Indore.name
    location                    = "eastus"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        environment = "Terraform Demo"
    }
}