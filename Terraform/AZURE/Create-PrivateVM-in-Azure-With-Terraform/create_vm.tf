# Create virtual machine
# Note:- if we want to genrate or connect private key then we will uncomment this resource

# resource "tls_private_key" "example_ssh" {
#   algorithm = "RSA"
#   rsa_bits = 4096
# }

# output "tls_private_key" { value = tls_private_key.example_ssh.private_key_pem }

resource "azurerm_linux_virtual_machine" "IndoreVM" {
    name                  = "MyVM"
    location              = "eastus"
    availability_set_id   = azurerm_availability_set.IndoreAVSet.id
    resource_group_name   = azurerm_resource_group.Indore.name
    network_interface_ids = [azurerm_network_interface.IndoreNIC.id]
    size                  = "Standard_B1s"

     source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }
    
    os_disk {
        name              = "myOsDisk"
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }
    
        computer_name  = "MyVM"
        admin_username = "abdul"
        admin_password = "Ansari@#33949"
        disable_password_authentication = false

    # admin_ssh_key {
    #     username       = "azureuser"
    #     public_key     = tls_private_key.example_ssh.public_key_openssh
    # }

    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.INDSACC.primary_blob_endpoint
    }

    tags = {
        environment = "Terraform Demo"
    }
}
