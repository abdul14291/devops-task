# Create subnet
resource "azurerm_subnet" "IndoreSubnet" {
    name                 = "IndoreMPSubnet"
    resource_group_name  = azurerm_resource_group.Indore.name
    virtual_network_name = azurerm_virtual_network.IndoreNetwork.name
    address_prefixes       = ["10.0.1.0/24"]
}