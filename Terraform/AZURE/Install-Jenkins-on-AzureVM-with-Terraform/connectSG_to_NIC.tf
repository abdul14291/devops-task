# Connect the security group to the network interface
# this is for attached our NIC with NSG
resource "azurerm_network_interface_security_group_association" "IndoreSGAssociate" {
    network_interface_id      = azurerm_network_interface.IndoreNIC.id
    network_security_group_id = azurerm_network_security_group.IndoreNSG.id
}