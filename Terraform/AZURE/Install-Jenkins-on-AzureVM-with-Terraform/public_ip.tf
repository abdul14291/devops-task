resource "azurerm_public_ip" "IndoreIP" {
  name                         = var.public_ip_name
  location                     = azurerm_resource_group.Indore.location
  resource_group_name          = azurerm_resource_group.Indore.name
  allocation_method            = "Static"
  domain_name_label            = var.public_domain_name
}