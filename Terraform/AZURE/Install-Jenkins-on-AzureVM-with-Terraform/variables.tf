### General Variables

variable "location" {
  description = "The data center location where all resources will be put into."
  default     = "eastus"
}

variable "resource_group_name" {
  description = "The name of the resource group which the Jenkins test farm will be created in."
  default     = "IndoreMP"
}


### Network Variables

variable "virtual_network_name" {
  description = "The name of the virtual network connecting all resources."
  default     = "IndoreMPNetwork"
}

variable "subnet_name" {
  description = "The name of the subnet where the Jenkins virtual machine will be put into."
  default     = "IndoreMPSubnet"
}

variable "public_ip_name" {
  description = "The name of the public IP address for Jenkins virtual machine."
  default     = "IndoreMPIP"
}

variable "public_domain_name" {
  description = "The domain name of the Jenkins virtual machine (without azure subdomain)."
  default     = "indore"
}

variable "network_security_group_name" {
  description = "The name of the network security group (firewall rules) for Jenkins virtual machine."
  default     = "IndoreMPNSG"
}

variable "network_interface_name" {
  description = "The name of the primary network interface which will be used by the Jenkins virtual machine."
  default     = "IndoreMPNIC"
}


### Virtual Machine Variables

variable "virtual_machine_name" {
  description = "The name of the Jenkins virtual machine which contains the Jenkins server."
  default     = "Indore-VM"
}

variable "virtual_machine_size" {
  description = "The size of the Jenkins virtual machine."
  # default     = "Standard_B1s"
  default     = "Standard_D2s_v3"
}

variable "virtual_machine_osdisk_name" {
  description = "The managed OS disk name of the Jenkins virtual machine."
  default     = "Indore-VM-OS-Disk"
}

variable "virtual_machine_osdisk_type" {
  description = "The managed OS disk type of the Jenkins virtual machine."
  default     = "Standard_LRS"
}

variable "virtual_machine_computer_name" {
  description = "The computer name of the Jenkins virtual machine."
  default     = "IndoreMP-VM"
}

variable "admin_username" {
  description = "The username of the administrator of the Jenkins virtual machine."
  default     = "abdul"
}

variable "admin_password" {
  description = "The password of the administrator of the Jenkins virtual machine."
  default     = "Indore@#2020"
}

variable "virtual_machine_extension" {
  default     = "jenkins_ext"
}

variable "ssh_public_key_data" {
  description = "The SSH public key for remote connection of the Jenkins virtual machine."
  # type = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC/bm+blWxQPk6SMkpxZ2ebqrdBdThfaMPVXQAruPfqnCflg8LxaK3Dp59Dag8/kHhvNyld91GZqkneyz05A86aGU5b2G2M7DgBjMYYwLBXKZ+f1NDeFSlBq+J0vBz9MJUXWErDVx4qk321rkWgZ2w5xYbwMhP9nAEkJEAf7Bs3kyXDpNlkYi/IQ8IvirCm7InjJvnXKi1xqM4Q33N7uYgcaJBauWPSexMvzGKEzkAJ4mC63nrS36UmAOR6DM9TwliSkIzJoUODuKA+X9ba2136xVLeW3+yNj3l64o5IApUV0T40lxLaWW/cw0lkur5aJrceDqOIHEcPmdf41J6Wauh abdul@ansari"
}

# variable "ssh_private_key_data" {
#   description = "The SSH private key for remote connection. It is used to configure the environment after the virtual machine is created."
#   type        = string
# }
