resource "azurerm_virtual_network" "IndoreNetwork" {
  name                = var.virtual_network_name
  location            = azurerm_resource_group.Indore.location
  resource_group_name = azurerm_resource_group.Indore.name
  address_space       = ["10.0.0.0/16"]
}