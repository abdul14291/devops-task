resource "azurerm_subnet" "IndoreSubnet" {
  name                 = var.subnet_name
  resource_group_name  = azurerm_resource_group.Indore.name
  address_prefixes       = ["10.0.1.0/24"]
  virtual_network_name = azurerm_virtual_network.IndoreNetwork.name
}