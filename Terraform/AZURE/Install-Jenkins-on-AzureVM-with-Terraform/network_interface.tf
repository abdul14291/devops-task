resource "azurerm_network_interface" "IndoreNIC" {
  name                      = var.network_interface_name
  location                  = azurerm_resource_group.Indore.location
  resource_group_name       = azurerm_resource_group.Indore.name

  ip_configuration {
    name                          = var.network_interface_name
    subnet_id                     = azurerm_subnet.IndoreSubnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.IndoreIP.id
  }
}