#!/bin/bash
#  Run me with superuser privileges
# echo 'abdul  ALL=(ALL:ALL) ALL' >> /etc/sudoers

# # Change file permision 
# sudo chmod 755 /etc/sudoers


sudo apt-get update

echo "Install Java JDK 8"
sudo apt-get install openjdk-8-jdk -y

# Install Jenkins
cd /tmp && wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add - || { throwError 'Install Jenkins wget'; }
echo 'deb https://pkg.jenkins.io/debian-stable binary/' | sudo tee -a /etc/apt/sources.list.d/jenkins.list || { throwError 'Install Jenkins tee'; }
sudo apt-get update || { throwError 'Install Jenkins update'; }
sudo apt-get install jenkins || { throwError 'Install Jenkins install'; }

echo "Password : "
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

# echo "Install Jenkins"
# usermod -aG sudo abdul
# wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
# sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
# /etc/apt/sources.list.d/jenkins.list'
# sudo apt-get update
# sudo apt-get install jenkins -y

#  echo "Chnage permision of Jenkins Password Directory"
# sudo chmod u+x /var/lib/jenkins/secrets

# echo "Enter in Jenkins Password Directory"
# sudo cd /var/lib/jenkins/secrets

# echo "Chnage permision of Jenkins Password file"
# sudo chmod u+x initialAdminPassword

# echo "Show Jenkins Password"
# sudo cat initialAdminPassword

# echo "Show Jenkins Password"
# sudo cat /var/lib/jenkins/secrets/initialAdminPassword

# echo "Stop Jenkins"
# sudo service jenkins stop

# echo "Enter in Temp Directory"
# cd /tmp

# echo "Move Jenkins file form tmp to /etc/defaults"
# sudo mv jenkins-new /etc/defaults

# echo "Moving into jenkins directory"
# cd /etc/default

# echo "Delete Jenkins Default file"
# sudo rm -rf jenkins

# echo "Rename Jenkins new file"
# sudo mv jenkins-new jenkins

# echo "Start Jenkins service"
# sudo service jenkins start


# sudo sh -c 'echo \"jenkins ALL=(ALL) NOPASSWD: ALL\" >> /etc/sudoers'
# http://ftp-chi.osuosl.org/pub/jenkins/war-stable/2.249.3/jenkins.war
# cd /home/abdul/Downloads
# java -jar jenkins.war --httpPort=3030