### Virtual Machine

resource "azurerm_virtual_machine" "vm" {
  name                  = var.virtual_machine_name
  location              = azurerm_resource_group.Indore.location
  resource_group_name   = azurerm_resource_group.Indore.name
  network_interface_ids = [azurerm_network_interface.IndoreNIC.id]
  vm_size               = var.virtual_machine_size

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = var.virtual_machine_osdisk_name
    create_option     = "FromImage"
    disk_size_gb      = 100
    managed_disk_type = var.virtual_machine_osdisk_type
  }

  os_profile {
    computer_name  = var.virtual_machine_computer_name
    admin_username = var.admin_username
    admin_password = var.admin_password
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

 connection {
    type        = "ssh"
    host        = azurerm_public_ip.IndoreIP.ip_address
    user        = var.admin_username
    password    = var.admin_password
    # timeout     = "15m"
  }

# Copies the myapp.conf file to /etc/myapp.conf

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install git",
      "sudo git clone https://github.com/abdul14291/install-jenkins-with-terraform.git",
      "cd install-jenkins-with-terraform",
      "chmod u+x jenkins_install.sh",
      "sh jenkins_install.sh",
    ]
}
  # provisioner "file" {
  #   source      = "jenkins-new"
  #   destination = "/etc/defaults"
  # }
}


# resource "azurerm_virtual_machine_extension" "jenkins_extenstion" {
#   name                 = var.virtual_machine_extension
#   virtual_machine_id   = azurerm_virtual_machine.vm.id
#   publisher            = "Microsoft.Azure.Extensions"
#   type                 = "CustomScript"
#   type_handler_version = "2.0"

#   connection {
#     type        = "ssh"
#     host        = azurerm_public_ip.IndoreIP.ip_address
#     user        = var.admin_username
#     password    = var.admin_password
#     # timeout     = "15m"
#   }

# # provisioner "remote-exec" {
# #     inline = [
# #       "sudo apt-get update",
# #       "sudo apt-get install git",
# #       "sudo git clone https://github.com/abdul14291/install-jenkins-with-terraform.git",
# #       "cd install-jenkins-with-terraform",
# #       "chmod +x jenkins_install.sh",
# #       "sh jenkins_install.sh",
# #     ]
# # }

# }